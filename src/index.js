/**
 * - Run `npx wrangler dev src/index.js` to start a development server
 * - Open a browser tab at http://localhost:8787/ to see worker in action
 * - Run `npx wrangler publish src/index.js --name trustsim` to publish worker
 *
 * Learn more at https://developers.cloudflare.com/workers/
 */
const accountId = 'e06399bb99a5c2ad13b88beb852cb3df';
const namespaceName = 'prisoners';

export default {
	async fetch(request, env, ctx) {
		// if GET /, return HTML for the mainpage
		// if GET /agent?agentname=<name>, call the user worker
		// if POST /, upload code as new user worker
		// if DELETE /, then delete user worker
		const url = new URL(request.url);
		let resp;

		if (request.method === 'GET' && url.pathname === '/'){
			return homepage();
		} if (request.method === 'GET' && url.pathname == '/agent'){
			resp = await callUserWorker(url.searchParams.get('agentname'))
		} if(request.method === 'POST') {
			resp = await createUserWorker(url.searchParams.get('agentname'), request.text())
		} else if (request.method === 'DELETE'){
			resp = await deleteUserWorker(url.searchParams.get('agentname'))
		}
		return new Response(resp);
		
	},
};

async function createUserWorker(workerName, content){
	let fullContent = `
"addEventListener('fetch', event => {
	event.respondWith(handleRequest(event.request));
})

async function handleRequest(request) {
	let previousGames = 
	return new Response(move(previousGames));
}

function move(previousGames){${content}}`;

	const url = `https://api.cloudflare.com/client/v4/accounts/${accountId}/workers/dispatch/namespaces/${namespaceName}/scripts/${workerName}`;
	const response = await fetch(url, {
		body: fullContent,
		method: 'PUT',
		headers: {
			'content-type': 'application/json;charset=UTF-8',
			'authorization': `Bearer ${token}`
		}
	});
}

async function callUserWorker(workerName){
	try {
		let userWorker = env.dispatcher.get(workerName);
		return await userWorker.fetch(request);
	} catch (e) {
		if(e.message.startsWith('Worker not found')) {
			// we tried to get a worker that doesn't exist in our dispatch namespace
			return new Response('', {status: 404})
		}

		// this could be any other exception from `fetch()` *or* an exception
		// thrown by the called worker (e.g. if the dispatched worker has 
		// `throw MyException()`, you could check for that here).
		return new Response(e.message, {status: 500})
	}
}

async function deleteUserWorker(workerName){
	const url = `https://api.cloudflare.com/client/v4/accounts/${accountId}/workers/dispatch/namespaces/${namespaceName}/scripts/${workerName}`;
	const response = await fetch(url, {
		method: 'DELETE',
		headers: {
			'content-type': 'application/json;charset=UTF-8',
			'authorization': `Bearer ${token}`
		}
	});
}

function homepage(){
	const html = `<!DOCTYPE html>
		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<script src="https://cdn.tailwindcss.com?plugins=forms,typography,aspect-ratio,line-clamp"></script>
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@shoelace-style/shoelace@2.0.0/dist/themes/light.css" />
			<script type="module" src="https://cdn.jsdelivr.net/npm/@shoelace-style/shoelace@2.0.0/dist/shoelace.js"></script>
		</head>
		<body>
		  <h1>PD Simulator</h1>
		</body>`;

	return new Response(html, {
		headers: {
			'content-type': 'text/html;charset=UTF-8',
		}
	});
}

function validateKey(key){

}