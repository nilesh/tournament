// utility function to choose one item randomly from array
const chooseRandom = (choices) => {
	return choices[Math.floor(Math.random() * choices.length)]
}

// game constants for Prisoners Dilemma
const PD = {
	COOPERATE: "COOPERATE",
	CHEAT: "CHEAT",
	NOISE: 0, // for simulating accidents/random mistakes/miscommunication
	PAYOFFS: {
		P: 0, // punishment: neither of you get anything
		S: -1, // sucker: you put in coin, other didn't.
		R: 2, // reward: you both put 1 coin in, both got 3 back
		T: 3 // temptation: you put no coin, got 3 coins anyway
	}
};

class Player {
	constructor(secret, description, strategy){
		this.secret = secret;
		this.description = description;
		this.strategy = strategy;
		this.score = 100;
	}

	move(previousGames){
		return this.strategy(previousGames);
	}

	resetScore(){
		this.score = 100;
	}
}

class Tournament {
	constructor(numTurns){
		this.numTurns = numTurns;
		this.players = [];
	}
	
	registerPlayer(player){
		this.players.push(player);
	}

	getPayoffs(move1, move2){
		if(move1==PD.CHEAT && move2==PD.CHEAT) 
			return [PD.PAYOFFS.P, PD.PAYOFFS.P]; // both punished
		
		if(move1==PD.COOPERATE && move2==PD.CHEAT) 
			return [PD.PAYOFFS.S, PD.PAYOFFS.T]; // sucker - temptation
		
		if(move1==PD.CHEAT && move2==PD.COOPERATE) 
			return [PD.PAYOFFS.T, PD.PAYOFFS.S]; // temptation - sucker
		
		if(move1==PD.COOPERATE && move2==PD.COOPERATE) 
			return [PD.PAYOFFS.R, PD.PAYOFFS.R]; // both rewarded
	}

	playTournament(){
		// Reset everyone's score
		this.players.forEach((p) => p.resetScore());
		
		// Round-robin
		this.players.forEach((first, i) => {
			this.players.slice(i+1).forEach((second) => {
				this.playRepeatedGame(first, second, this.numTurns);
			});
		});

		console.log("\n\nHere are the final scores:");
		this.players.forEach((p) => console.log(p.secret, p.description, p.score));
	}

	playRepeatedGame(p1, p2, turns){
		// p1.sendMessage('starting repeatedGame');
		// p2.sendMessage('starting repeatedGame');

		console.log(`\nstarting ${turns} iterated games between`, p1.description, p2.description, "\n");
		let previousGames = [];

		for(let i = 0; i < turns; i++){
			let moves = this.playOneGame(p1, p2, previousGames);
			previousGames.push(moves);
			console.log({moves});
			console.log(`scores: ${p1.score}, ${p2.score}`);
		} 
	}

	playOneGame(p1, p2, previousGames){
		console.log(`\nplaying ${p1.description} against ${p2.description}`);
		
		// pick your moves
		let m1 = p1.move(previousGames);
		let m2 = p2.move(previousGames.map(g => [...g].reverse()));

		// Noise: random mistakes, flip around!
		if(Math.random()<PD.NOISE) m1 = ((m1==PD.COOPERATE) ? PD.CHEAT : PD.COOPERATE);
		if(Math.random()<PD.NOISE) m2 = ((m2==PD.COOPERATE) ? PD.CHEAT : PD.COOPERATE);
		
		let result = this.getPayoffs(m1, m2);

		// update score
		p1.score += result[0];
		p2.score += result[1];
		
		return [m1, m2];
	}
}

let alwaysCooperate = () => { return PD.COOPERATE; }
let alwaysCheat = () => { return PD.CHEAT; }
let alwaysRandom = () => { return chooseRandom([PD.COOPERATE, PD.CHEAT]); }

let titForTat = (previousGames) => {
	// start with cooperate, then keep copying the opponent
	if(previousGames.length == 0) return PD.COOPERATE;
	return previousGames[previousGames.length - 1][1]; // 0 was self move, 1 was opponent's
}

let titForTwoTats = (previousGames) => {
	// start with cooperate, then retaliate ONLY after two betrayals
	// console.log({previousGames});
	if(previousGames.filter((g) => g[1] == PD.CHEAT).length >= 2) return PD.CHEAT;
	return PD.COOPERATE;
}

// other strategies named here: https://scriptim.github.io/PrisonersDilemma/

let t = new Tournament(5);

for(let i=0; i < 4; i++){
	// t.registerPlayer(new Player(`p${i}`, `p${i}: alwaysRandom`, alwaysRandom))
	t.registerPlayer(new Player(`q${i}`, `q${i}: titForTat`, titForTat))
	// t.registerPlayer(new Player(`r${i}`, `r${i}: alwaysCooperate`, alwaysCooperate))
	t.registerPlayer(new Player(`s${i}`, `s${i}: alwaysCheat`, alwaysCheat))
}

t.playTournament();